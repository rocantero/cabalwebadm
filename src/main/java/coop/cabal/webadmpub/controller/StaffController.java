package coop.cabal.webadmpub.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.cabal.webadmpub.entity.Content;
import coop.cabal.webadmpub.entity.Staff;
import coop.cabal.webadmpub.repository.StaffRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/staff")
public class StaffController {

   @Autowired
   private StaffRepository repository;

   @GetMapping
   public Iterable<Staff> findAll() {
	   return repository.findAllOrderByPositionAsc();
   }
   
   @GetMapping(path = "/find/{term}")
   public Iterable<Staff> findByTerm(@PathVariable("term") String term) {
//	   Staff cont = new Staff();
//	   cont.setDivision(term);
//	   Example<Staff> exa = Example.of(cont);
	   return repository.findAllByDivisionOrderByPositionAsc(term);
   }

   @GetMapping(path = "/{id}")
   public Optional<Staff> find(@PathVariable("id") Integer id) {
       return repository.findById(id);
   }
}