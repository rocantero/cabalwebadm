package coop.cabal.webadmpub.controller;
import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.cabal.webadmpub.entity.CcReqFormEntry;
import coop.cabal.webadmpub.entity.CcReqFormEvent;
import coop.cabal.webadmpub.repository.CcReqFormEventRepository;
import coop.cabal.webadmpub.repository.CcReqFormRepository;
import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/forms/quiero-tc")
public class CcReqFormController {
	@Autowired
		private CcReqFormRepository repository;
	@Autowired
		private CcReqFormEventRepository evRepository;

	   @PostMapping(consumes = "application/json")
	   public CcReqFormEntry create(@RequestBody CcReqFormEntry faq) {
		   // TODO obtener IP de remitente para agregar al evento
		   faq.setStatus("Pendiente");
	       CcReqFormEntry result = repository.save(faq);
	       CcReqFormEvent ev = new CcReqFormEvent();
	       ev.setEvent("Formulario enviado");
	       ev.setCreated_at(LocalDateTime.now());
	       ev.setForm(result);
	       evRepository.save(ev);
	       return result;
	   }
}
