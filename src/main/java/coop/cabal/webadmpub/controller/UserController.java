package coop.cabal.webadmpub.controller;
// package coop.cabal.webadmpub.controller;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.web.bind.annotation.DeleteMapping;
// import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.PathVariable;
// import org.springframework.web.bind.annotation.PostMapping;
// import org.springframework.web.bind.annotation.PutMapping;
// import org.springframework.web.bind.annotation.RequestBody;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RestController;

// import coop.cabal.webadmpub.entity.User;
// import coop.cabal.webadmpub.repository.UserRepository;

// import javassist.tools.web.BadHttpRequest;

// @RestController
// @RequestMapping(path = "/users")
// public class UserController {

//    @Autowired
//    private UserRepository repository;

//    @GetMapping
//    public Iterable<User> findAll() {
//        return repository.findAll();
//    }

//    @GetMapping(path = "/{userId}")
//    public User find(@PathVariable("userId") Double userId) {
//        return repository.findOne(userId);
//    }

//    @PostMapping(consumes = "application/json")
//    public User create(@RequestBody User user) {
//        return repository.save(user);
//    }

//    @DeleteMapping(path = "/{userId}")
//    public void delete(@PathVariable("userId") Double userId) {
//        repository.deleteById(userId);
//    }

//    @PutMapping(path = "/{userId}")
//    public User update(@PathVariable("userId") Double userId, @RequestBody User user) throws BadHttpRequest {
//        if (repository.existsById(userId)) {
//            user.setuserId(userId);
//            return repository.save(user);
//        } else {
//            throw new BadHttpRequest();
//        }
//    }

// }