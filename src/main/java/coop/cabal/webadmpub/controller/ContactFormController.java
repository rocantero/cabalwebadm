package coop.cabal.webadmpub.controller;
import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.cabal.webadmpub.entity.CcReqFormEntry;
import coop.cabal.webadmpub.entity.CcReqFormEvent;
import coop.cabal.webadmpub.entity.ContactFormEntry;
import coop.cabal.webadmpub.entity.ContactFormEvent;
import coop.cabal.webadmpub.repository.ContactFormEventRepository;
import coop.cabal.webadmpub.repository.ContactFormRepository;
import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/forms/contact")
public class ContactFormController {
	@Autowired
	   private ContactFormRepository repository;
	   private ContactFormEventRepository evRepository;

	   @PostMapping(consumes = "application/json")
	   public ContactFormEntry create(@RequestBody ContactFormEntry faq) {
		   faq.setStatus("Pendiente");
	       
	       ContactFormEntry result = repository.save(faq);
	       ContactFormEvent ev = new ContactFormEvent();
	       ev.setEvent("Formulario enviado");
	       ev.setCreated_at(LocalDateTime.now());
	       ev.setForm(result);
	       evRepository.save(ev);
	       return result;
	   }
}
