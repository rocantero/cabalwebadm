package coop.cabal.webadmpub.controller;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.cabal.webadmpub.entity.ContactFormEntry;
import coop.cabal.webadmpub.entity.ContactFormEvent;
import coop.cabal.webadmpub.entity.JoinCoopFormEntry;
import coop.cabal.webadmpub.entity.JoinCoopFormEvent;
import coop.cabal.webadmpub.repository.JoinCoopFormEventRepository;
import coop.cabal.webadmpub.repository.JoinCoopFormRepository;

@RestController
@RequestMapping(path = "/forms/quiero-asociarme")
public class JoinCoopFormController {
	@Autowired
	   private JoinCoopFormRepository repository;
		private JoinCoopFormEventRepository evRepository;

	   @PostMapping(consumes = "application/json")
	   public JoinCoopFormEntry create(@RequestBody JoinCoopFormEntry faq) {
	       faq.setStatus("Pendiente");
	       JoinCoopFormEntry result = repository.save(faq);
	       JoinCoopFormEvent ev = new JoinCoopFormEvent();
	       ev.setEvent("Formulario enviado");
	       ev.setCreated_at(LocalDateTime.now());
	       ev.setForm(result);
	       evRepository.save(ev);
	       return result;
	   }
}
