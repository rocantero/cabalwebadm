package coop.cabal.webadmpub.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.cabal.webadmpub.entity.Deal;
import coop.cabal.webadmpub.repository.DealsRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/deals")
public class DealsController {

   @Autowired
   private DealsRepository repository;

   @GetMapping
   public Iterable<Deal> findAll() {
       return repository.findAll();
   }

   @GetMapping(path = "/{id}")
   public Optional<Deal> find(@PathVariable("id") Integer id) {
       return repository.findById(id);
   }


}