package coop.cabal.webadmpub.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.cabal.webadmpub.entity.Content;
import coop.cabal.webadmpub.repository.ContentRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/content")
public class ContentController {

   @Autowired
   private ContentRepository repository;

   @GetMapping
   public Iterable<Content> findAll() {
       return repository.findAll();
   }

   @GetMapping(path = "/{id}")
   public Optional<Content> find(@PathVariable("id") String id) {
       return repository.findById(id);
   }

   @GetMapping(path = "/find/{term}")
   public Iterable<Content> findByTerm(@PathVariable("term") String term) {
	   Content cont = new Content();
	   cont.setSection(term);
	   Example<Content> exa = Example.of(cont);
	   return repository.findAll(exa);
   }

}