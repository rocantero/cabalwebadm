package coop.cabal.webadmpub.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.cabal.webadmpub.entity.Faq;
import coop.cabal.webadmpub.repository.FaqRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/faqs")
public class FaqController {

   @Autowired
   private FaqRepository repository;

   @GetMapping
   public Iterable<Faq> findAll() {
       return repository.findAll();
   }

   @GetMapping(path = "/{id}")
   public Optional<Faq> find(@PathVariable("id") Integer id) {
       return repository.findById(id);
   }
   
   @GetMapping(path = "/category/{term}")
   public Iterable<Faq> findByTerm(@PathVariable("term") String term) {
	   Faq faq = new Faq();
	   faq.setCategory(term);
	   Example<Faq> exa = Example.of(faq);
	   return repository.findAll(exa);
   }

}