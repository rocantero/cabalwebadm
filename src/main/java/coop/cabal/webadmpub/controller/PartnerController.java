package coop.cabal.webadmpub.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.cabal.webadmpub.entity.Partner;
import coop.cabal.webadmpub.repository.PartnerRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/partners")
public class PartnerController {

   @Autowired
   private PartnerRepository repository;

   @GetMapping
   public Iterable<Partner> findAll() {
	   return repository.findAllOrderByPositionAsc();
   }

   @GetMapping(path = "/{id}")
   public Optional<Partner> find(@PathVariable("id") Integer id) {
       return repository.findById(id);
   }

}