package coop.cabal.webadmpub.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.cabal.webadmpub.entity.Faq;
import coop.cabal.webadmpub.entity.News;
import coop.cabal.webadmpub.repository.NewsRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/news")
public class NewsController {

   @Autowired
   private NewsRepository repository;

   @GetMapping
   public Iterable<News> findAll() {
       return repository.findAll();
   }

   @GetMapping(path = "/{id}")
   public Optional<News> find(@PathVariable("id") Integer id) {
       return repository.findById(id);
   }
   
   @GetMapping(path = "/slide")
   public News findSlide() {
	   News news = new News();
	   news.setIsSlide(true);
	   Example<News> exa = Example.of(news);
	   List<News> result = repository.findAll(exa);
	   if(result.size()>0) {
		   News slide = result.get(0);
		   return slide;
	   }
	return null;
   }

}