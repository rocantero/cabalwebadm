package coop.cabal.webadmpub.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.cabal.webadmpub.entity.Slide;
import coop.cabal.webadmpub.repository.SlideRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/slides")
public class SlideController {

   @Autowired
   private SlideRepository repository;

   @GetMapping
   public Iterable<Slide> findAll() {
       return repository.findAllOrderByPositionAsc();
   }

   @GetMapping(path = "/{id}")
   public Optional<Slide> find(@PathVariable("id") Integer id) {
       return repository.findById(id);
   }
}