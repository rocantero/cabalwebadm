package coop.cabal.webadmpub.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import coop.cabal.webadmpub.entity.Faq;

@RepositoryRestResource(path="/faq")
public interface FaqRepository extends JpaRepository<Faq, Integer> {

}