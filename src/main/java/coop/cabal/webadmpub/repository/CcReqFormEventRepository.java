package coop.cabal.webadmpub.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import coop.cabal.webadmpub.entity.CcReqFormEvent;

public interface CcReqFormEventRepository extends JpaRepository<CcReqFormEvent, Integer> {

}