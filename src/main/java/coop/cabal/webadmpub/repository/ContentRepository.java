package coop.cabal.webadmpub.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import coop.cabal.webadmpub.entity.Content;

@RepositoryRestResource(path="/content")
public interface ContentRepository extends JpaRepository<Content, String> {

}