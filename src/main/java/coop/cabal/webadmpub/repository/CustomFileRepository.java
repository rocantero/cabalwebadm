package coop.cabal.webadmpub.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import coop.cabal.webadmpub.entity.CustomFile;

@RepositoryRestResource(path="/files")
public interface CustomFileRepository extends JpaRepository<CustomFile, String> {

}