package coop.cabal.webadmpub.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import coop.cabal.webadmpub.entity.Staff;

@RestResource(exported=false)
public interface StaffRepository extends JpaRepository<Staff, Integer> {
	@Query("FROM Slide ORDER BY position ASC")
	Iterable<Staff> findAllOrderByPositionAsc();
	
	Iterable<Staff> findAllByDivisionOrderByPositionAsc(String division);
}