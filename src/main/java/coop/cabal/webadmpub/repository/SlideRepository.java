package coop.cabal.webadmpub.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import coop.cabal.webadmpub.entity.Slide;

//@RepositoryRestResource(path="/slides")
@RestResource(exported=false)
public interface SlideRepository extends JpaRepository<Slide, Integer> {
	@Query("FROM Slide ORDER BY position ASC")
	Iterable<Slide> findAllOrderByPositionAsc();

}