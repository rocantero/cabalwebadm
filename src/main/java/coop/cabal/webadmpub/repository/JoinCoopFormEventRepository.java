package coop.cabal.webadmpub.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import coop.cabal.webadmpub.entity.JoinCoopFormEvent;

public interface JoinCoopFormEventRepository extends JpaRepository<JoinCoopFormEvent, Integer> {

}