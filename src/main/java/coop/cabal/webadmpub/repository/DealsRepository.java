package coop.cabal.webadmpub.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import coop.cabal.webadmpub.entity.Deal;

public interface DealsRepository extends JpaRepository<Deal, Integer>   {

}
