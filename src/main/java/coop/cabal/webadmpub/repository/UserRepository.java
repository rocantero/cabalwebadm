package coop.cabal.webadmpub.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import coop.cabal.webadmpub.entity.User;

@RepositoryRestResource(path="/users")
public interface UserRepository extends JpaRepository<User, Integer> {

}