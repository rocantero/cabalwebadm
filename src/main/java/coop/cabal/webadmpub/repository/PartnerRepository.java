package coop.cabal.webadmpub.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import coop.cabal.webadmpub.entity.Partner;

@RepositoryRestResource(path="/partners")
public interface PartnerRepository extends JpaRepository<Partner, Integer> {
	@Query("FROM Partner ORDER BY position ASC")
	Iterable<Partner> findAllOrderByPositionAsc();
}