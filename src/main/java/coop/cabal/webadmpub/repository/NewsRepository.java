package coop.cabal.webadmpub.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import coop.cabal.webadmpub.entity.News;

@RepositoryRestResource(path="/news")
public interface NewsRepository extends JpaRepository<News, Integer> {

}