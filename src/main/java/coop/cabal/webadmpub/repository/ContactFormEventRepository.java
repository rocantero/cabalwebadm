package coop.cabal.webadmpub.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import coop.cabal.webadmpub.entity.ContactFormEvent;

public interface ContactFormEventRepository extends JpaRepository<ContactFormEvent, Integer> {

}