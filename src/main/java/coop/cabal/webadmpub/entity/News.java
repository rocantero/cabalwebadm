package coop.cabal.webadmpub.entity;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.hibernate.annotations.Type;

@Entity
public class News {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer id;
	public String title;
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	public String content;
	public String headerImage;
	public String thumbnail;
	public Boolean isSlide;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getHeaderImage() {
		return headerImage;
	}
	public void setHeaderImage(String headerImage) {
		this.headerImage = headerImage;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public Boolean getIsSlide() {
		return isSlide;
	}
	public void setIsSlide(Boolean isSlide) {
		this.isSlide = isSlide;
	}
}
