package coop.cabal.webadmpub.entity;

import java.util.Date;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.hibernate.annotations.Type;

@Entity
public class Deal {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String title;
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String description;
    private Date startDate;
    private Date endDate;
    private String backerEntity;
    private String image;
    
	public Integer getId() {
		return id;
	}
	public String getTitle() {
		return title;
	}
	public String getDescription() {
		return description;
	}
	public Date getStartDate() {
		return startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public String getBackerEntity() {
		return backerEntity;
	}
	public String getImage() {
		return image;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public void setBackerEntity(String backerEntity) {
		this.backerEntity = backerEntity;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
}


