package coop.cabal.webadmpub.entity;

	import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

	@Entity
	public class Content {
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
	    private String identifier;
	    private String section;
	    private String text;
	    
	    @CreatedBy
	    protected String created_by;

	    @CreatedDate
	    @Temporal(TemporalType.TIMESTAMP)
	    protected Date created_at;

	    @LastModifiedBy
	    protected String modified_by;

	    @LastModifiedDate
	    @Temporal(TemporalType.TIMESTAMP)
	    protected Date modified_at;
	    
		public String getIdentifier() {
			return identifier;
		}
		public void setIdentifier(String identifier) {
			this.identifier = identifier;
		}
		public String getSection() {
			return section;
		}
		public void setSection(String section) {
			this.section = section;
		}
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
	    
		
	}


