package coop.cabal.webadmpub.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer","handler","form"})
public class JoinCoopFormEvent {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private LocalDateTime created_at;
	private String event;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn
	private JoinCoopFormEntry form;
	public Integer getId() {
		return id;
	}
	public LocalDateTime getCreated_at() {
		return created_at;
	}
	public String getEvent() {
		return event;
	}
	public JoinCoopFormEntry getForm() {
		return form;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setCreated_at(LocalDateTime created_at) {
		this.created_at = created_at;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public void setForm(JoinCoopFormEntry form) {
		this.form = form;
	}
	
}
