package coop.cabal.webadmpub.entity;

	import javax.persistence.Entity;
	import javax.persistence.Id;

	@Entity
	public class CustomFile {
		@Id
	    private Double id;
	    private String filename;
	    private String format;
	    private String url;
	    
		public Double getId() {
			return id;
		}
		public void setId(Double id) {
			this.id = id;
		}
		public String getFilename() {
			return filename;
		}
		public void setFilename(String filename) {
			this.filename = filename;
		}
		public String getFormat() {
			return format;
		}
		public void setFormat(String format) {
			this.format = format;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String utl) {
			this.url = utl;
		}
	    
		
	}


