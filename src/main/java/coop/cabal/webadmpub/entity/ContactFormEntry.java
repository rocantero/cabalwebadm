package coop.cabal.webadmpub.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
@Entity
public class ContactFormEntry {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
    private String firstName;
    private String lastName;
    private String telephone;
    private String email;
    private String area;
    private String message;
    private String assignedTo;
    private String status;
    
    @CreatedBy
    protected String created_by;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    protected Date created_at;

    @LastModifiedBy
    protected String modified_by;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    protected Date modified_at;
    
    @PrePersist
    private void prePersistFunction(){
    	this.created_at = new Date();
    }
    
    public Integer getId() {
		return id;
	}
    public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getTelephone() {
		return telephone;
	}

	public String getEmail() {
		return email;
	}

	public String getArea() {
		return area;
	}

	public String getMessage() {
		return message;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public String getStatus() {
		return status;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public void setStatus(String status) {
		this.status = status;
	}
    
    
}
