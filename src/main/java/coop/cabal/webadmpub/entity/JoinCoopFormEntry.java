package coop.cabal.webadmpub.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@EntityListeners(AuditingEntityListener.class)
@Entity
public class JoinCoopFormEntry {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
    private String firstName;
    private String lastName;
    private String telephone;
    private String email;
    private String message;
    private String document;
    private String coop;
    
    private String assignedTo;
    private String status;
    
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    protected Date created_at;
    
    @PrePersist
    private void prePersistFunction(){
    	this.created_at = new Date();
    }
    
    public Integer getId() {
		return id;
	}
    public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getTelephone() {
		return telephone;
	}

	public String getEmail() {
		return email;
	}

	public String getMessage() {
		return message;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	public String getAssignedTo() {
		return assignedTo;
	}
	public String getStatus() {
		return status;
	}
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDocument() {
		return document;
	}
	public String getCoop() {
		return coop;
	}
	public void setDocument(String document) {
		this.document = document;
	}
	public void setCoop(String coop) {
		this.coop = coop;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
}
