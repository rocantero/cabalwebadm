package coop.cabal.webadmpub.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;


@Entity
@Table(name="user",schema="public")
public class User {

   @Id
   private String username;
   private String password;
   private String name;
   private String email;

   @CreatedBy
   protected String created_by;

   @CreatedDate
   @Temporal(TemporalType.TIMESTAMP)
   protected Date created_at;

   @LastModifiedBy
   protected String modified_by;

   @LastModifiedDate
   @Temporal(TemporalType.TIMESTAMP)
   protected Date modified_at;
   

   public String getUsername() {
      return username;
   }

   public void setUsername(String username) {
      this.username = username;
   }

   public String getPassword() {
      return password;
   }

   public void setPassword(String password) {
      this.password = password;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

}