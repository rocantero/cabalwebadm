--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: content; Type: TABLE; Schema: public; Owner: cblwebadm
--

CREATE TABLE public.content (
    identifier character varying(255) NOT NULL,
    section character varying(255),
    text text,
    created_at timestamp with time zone,
    modified_at timestamp with time zone,
    created_by character varying(100),
    modified_by character varying(100)
);


ALTER TABLE public.content OWNER TO cblwebadm;

--
-- Name: custom_file; Type: TABLE; Schema: public; Owner: cblwebadm
--

CREATE TABLE public.custom_file (
    id double precision NOT NULL,
    filename character varying(255),
    format character varying(255),
    url character varying(255)
);


ALTER TABLE public.custom_file OWNER TO cblwebadm;

--
-- Name: customfile; Type: TABLE; Schema: public; Owner: cblwebadm
--

CREATE TABLE public.customfile (
    id bigint NOT NULL,
    filename character varying(255),
    format character varying(255),
    url character varying(255)
);


ALTER TABLE public.customfile OWNER TO cblwebadm;

--
-- Name: faq; Type: TABLE; Schema: public; Owner: cblwebadm
--

CREATE TABLE public.faq (
    id bigint NOT NULL,
    question character varying(255),
    answer text,
    "order" integer,
    category character varying(255),
    created_at timestamp with time zone,
    modified_at timestamp with time zone,
    created_by character varying(100),
    modified_by character varying(100)
);


ALTER TABLE public.faq OWNER TO cblwebadm;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: cblwebadm
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO cblwebadm;

--
-- Name: news; Type: TABLE; Schema: public; Owner: cblwebadm
--

CREATE TABLE public.news (
    id double precision NOT NULL,
    content character varying(255),
    header_image character varying(255),
    title character varying(255),
    created_at timestamp with time zone,
    modified_at timestamp with time zone,
    created_by character varying(100),
    modified_by character varying(100)
);


ALTER TABLE public.news OWNER TO cblwebadm;

--
-- Name: partner; Type: TABLE; Schema: public; Owner: cblwebadm
--

CREATE TABLE public.partner (
    id bigint NOT NULL,
    name character varying(255),
    url character varying(255),
    logo character varying(255),
    "position" integer,
    created_at timestamp with time zone,
    modified_at timestamp with time zone,
    created_by character varying(100),
    modified_by character varying(100)
);


ALTER TABLE public.partner OWNER TO cblwebadm;

--
-- Name: slide; Type: TABLE; Schema: public; Owner: cblwebadm
--

CREATE TABLE public.slide (
    title character varying(255) NOT NULL,
    image character varying(255),
    link character varying(255),
    subtitle character varying(255),
    id bigint NOT NULL,
    "order" integer,
    created_at timestamp with time zone,
    modified_at timestamp with time zone,
    created_by character varying(100),
    modified_by character varying(100)
);


ALTER TABLE public.slide OWNER TO cblwebadm;

--
-- Name: staff; Type: TABLE; Schema: public; Owner: cblwebadm
--

CREATE TABLE public.staff (
    id double precision NOT NULL,
    division character varying(255),
    name character varying(255),
    picture character varying(255),
    "position" character varying(255),
    created_at timestamp with time zone,
    modified_at timestamp with time zone,
    created_by character varying(100),
    modified_by character varying(100)
);


ALTER TABLE public.staff OWNER TO cblwebadm;

--
-- Name: user; Type: TABLE; Schema: public; Owner: cblwebadm
--

CREATE TABLE public."user" (
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    name character varying(255),
    email character varying(255),
    created_at timestamp with time zone,
    modified_at timestamp with time zone,
    created_by character varying(100),
    modified_by character varying(100)
);


ALTER TABLE public."user" OWNER TO cblwebadm;

--
-- Data for Name: content; Type: TABLE DATA; Schema: public; Owner: cblwebadm
--

COPY public.content (identifier, section, text, created_at, modified_at, created_by, modified_by) FROM stdin;
micabal	micabal	<h1 class="text-primary titulo-seccion" style="font-family: klavika, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 1.955rem; width: 1582.8px; border-bottom: 3px solid; color: rgb(0, 83, 139) !important;">Requisitos para acceder a una Tarjeta de Crédito Cabal</h1><p style="color: rgb(33, 37, 41); font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px;">Cabal emite tarjetas a través de sus Entidades Emisoras. Las condiciones pueden variar de acuerdo a cada Entidad Emisora (*). Los requisitos más comunes son:</p><ul style="color: rgb(33, 37, 41); font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px;"><li>Documento de Identidad</li><li>Boleta de Servicios Públicos: no superior a 90 días de su emisión.</li><li>Si trabajas en relación de dependencia: 3 últimos recibos de sueldo.</li><li>Si trabajas de forma independiente: Últimos 3 pagos de I.V.A.</li><li>No figurar con resultado negativo en la base de datos de Inforconf</li><li>Si sos Titular de una Tarjeta de Crédito: Extracto de cuenta, con antigüedad superior a 6 meses.</li><li>Tus Líneas serán asignadas de acuerdo a los documentos que presentaste y estarán sujetos a la verificación que realice tu Entidad Emisora</li></ul><p style="color: rgb(33, 37, 41); font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px;"><small style="font-size: 14.72px;">(*) Los mismos no son taxativos y se encuentran sujetos a lo establecido por cada Entidad Emisora</small></p><p style="color: rgb(33, 37, 41); font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px;"><span style="font-weight: bolder;">¿Qué límites me otorgarán? (*)</span></p><p style="color: rgb(33, 37, 41); font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px;">Los límite serán asignados de acuerdo a los documentos/requisitos que presentaste, que avalen tus ingresos y estarán sujetos a una verificación que realiza la Entidad Emisora de donde solicitaste tu Tarjeta CABAL, para medir la capacidad de pago.<br><small style="font-size: 14.72px;">(*) Los mismos no son taxativos y se encuentran sujetos a lo establecido por cada Entidad Emisora</small></p>	\N	\N	\N	\N
cooperativas	comercios	<h5 class="card-title" style="font-family: klavika, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; color: rgb(33, 37, 41); font-size: 1.265rem;">Requisitos</h5><ol style="color: rgb(33, 37, 41); font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px;"><li>Fotocopias de la Cedula de identidad del o los Representantes Legales (Vigentes).</li><li>Fotocopia del R.U.C (Registro Único de Contribuyentes)</li><li>Estatutos Sociales</li><li>Copia de Actas Ultima Asamblea en caso de Elección (cambio) de Autoridades firmantes</li><li>Poderes de administración, para los casos en que se cuente con la firma de un Apoderado Legal</li></ol><p class="card-text mt-4" style="color: rgb(33, 37, 41); font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px;">Si desea pre-adherirse en linea podrá completando el siguiente&nbsp;<a href="https://www.cabal.coop.py/formularios/adherite" id="ember592" class="ember-view" style="outline: 0px; color: rgb(0, 83, 139);">formulario&nbsp;</a>y concretar una venta virtual. El importe correspondiente será acumulado en una cuenta transitoria hasta en tanto se confirmen todos los documentos solicitados que respaldan la formalidad del Comercio. O de lo contrario si desea que lo visite un representante comercial indique&nbsp;<a href="https://www.cabal.coop.py/formularios/contacto" id="ember601" class="ember-view" style="outline: 0px; color: rgb(0, 83, 139);">aquí&nbsp;</a>sus datos y nos pondremos en contacto con usted.</p><div class="col-md-8" style="width: 1027.19px; min-height: 1px; padding-right: 15px; padding-left: 15px; flex-basis: 66.6667%; max-width: 66.6667%; color: rgb(33, 37, 41); font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px; margin: 40px auto;"><div class="row" style="margin-right: -15px; margin-left: -15px;"></div></div>	\N	\N	\N	\N
encabezado	comercios	<h4 style="font-family: klavika, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; color: rgb(33, 37, 41); font-size: 1.38rem;">Documentos necesarios para que puedas vender con Cabal</h4><p style="color: rgb(33, 37, 41); font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px;">Las documentaciones que deberán acompañar a El Contrato de Productos y Servicios Cabal, mínimamente necesarios exigibles a los Establecimientos Comerciales se clasifican según la constitución de las mismas y son:</p>	\N	\N	\N	\N
sociedades	comercios	<h5 class="card-title" style="font-family: klavika, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; color: rgb(33, 37, 41); font-size: 1.265rem;">Requisitos</h5><ol style="color: rgb(33, 37, 41); font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px;"><li>Fotocopias de la Cedulas de identidad del o los Representantes Legales (Vigentes)</li><li>Fotocopia del R.U.C (Registro Único de Contribuyentes)</li><li>Copia de la Constitución de Sociedad</li><li>Copia de Modificación de Estatutos en caso de que haya habido cambio de los Representantes Legales.</li><li>Copia Actas Ultima Asamblea en caso de cambio de autoridades firmantes</li><li>Poderes de administración, para los casos en que se cuente con la firma de un Apoderado Legal</li></ol><p class="card-text mt-4" style="margin-bottom: 1rem; color: rgb(33, 37, 41); font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px;">Si desea pre-adherirse en linea podrá completando el siguiente&nbsp;<a href="https://www.cabal.coop.py/formularios/adherite" id="ember592" class="ember-view" style="outline: 0px; color: rgb(0, 83, 139);">formulario&nbsp;</a>y concretar una venta virtual. El importe correspondiente será acumulado en una cuenta transitoria hasta en tanto se confirmen todos los documentos solicitados que respaldan la formalidad del Comercio. O de lo contrario si desea que lo visite un representante comercial indique&nbsp;<a href="https://www.cabal.coop.py/formularios/contacto" id="ember601" class="ember-view" style="outline: 0px; color: rgb(0, 83, 139);">aquí&nbsp;</a>sus datos y nos pondremos en contacto con usted.</p>	\N	\N	\N	\N
unipersonales	comercios	<h5 class="card-title" style="font-family: klavika, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; color: rgb(33, 37, 41); font-size: 1.265rem;">Requisitos</h5><ol style="color: rgb(33, 37, 41); font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px;"><li>Fotocopias de la Cedula de identidad del Propietario</li><li>Fotocopia del R.U.C (Registro Único de Contribuyentes)</li></ol><p class="card-text mt-4" style="margin-bottom: 1rem; color: rgb(33, 37, 41); font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px;">Si desea pre-adherirse en linea podrá completando el siguiente&nbsp;<a href="https://www.cabal.coop.py/formularios/adherite" id="ember592" class="ember-view" style="outline: 0px; color: rgb(0, 83, 139);">formulario&nbsp;</a>y concretar una venta virtual. El importe correspondiente será acumulado en una cuenta transitoria hasta en tanto se confirmen todos los documentos solicitados que respaldan la formalidad del Comercio. O de lo contrario si desea que lo visite un representante comercial indique&nbsp;<a href="https://www.cabal.coop.py/formularios/contacto" id="ember601" class="ember-view" style="outline: 0px; color: rgb(0, 83, 139);">aquí&nbsp;</a>sus datos y nos pondremos en contacto con usted.</p>	\N	\N	\N	\N
cabalenpy	institucional	<section class="container mt-4 mb-4" style="padding-right: 15px; padding-left: 15px; width: 1612.8px; max-width: 96%; color: rgb(33, 37, 41); font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px;"><div id="historia" class="row" style="margin-right: -15px; margin-left: -15px; margin-top: 50px;"><div class="col-md-12" style="width: 1612.8px; min-height: 1px; padding-right: 15px; padding-left: 15px;"><p class="text-justify">El&nbsp;<span class="text-primary" style="font-weight: bolder; color: rgb(0, 83, 139) !important;">Sistema CABAL de Tarjetas de Crédito</span>&nbsp;fue fundado el 20 de noviembre de 1980. Nuestra marca, está operando en el Paraguay desde 1989 y se consolido nuestra presencia aún más desde hace 19 años al constituirse la Cooperativa Binacional de Servicios Cabal Paraguay Ltda. como licenciataria de la marca, fruto de la asociación entre la Cooperativa Universitaria y Cabal Argentina.<br>En la actualidad nuestra marca se ha posicionado como la principal del Sector Cooperativo en Paraguay, pues prácticamente el 50% de las Tarjetas emitidas a socios cooperativistas en el país son nuestras. El 80% de las Cooperativas que emiten tarjetas de crédito trabajan con nosotros.</p></div></div><h2 class="text-primary" style="font-family: klavika, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 1.725rem; color: rgb(0, 83, 139) !important;">Visión</h2><p>Ser la Empresa Líder en la Provisión de Soluciones de Medios de Pago del País</p><h2 class="text-primary" style="font-family: klavika, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 1.725rem; color: rgb(0, 83, 139) !important;">Misión</h2><p>Colaborar con el cumplimiento de los objetivos de nuestros clientes a través de la provisión de soluciones de medios de pago innovadoras, desarrolladas en conjunto entre nuestros aliados estratégicos y el equipo de trabajo.</p><h2 class="text-primary" style="font-family: klavika, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 1.725rem; color: rgb(0, 83, 139) !important;">Valores Corporativos</h2><ul class="lista-cabal"><li>Confianza y Confidencialidad</li><li>Responsabilidad y Cumplimiento</li><li>Foco en el Servicio y en el Cliente</li><li>Calidad</li></ul></section>	\N	\N	\N	\N
nuestracompania	home	<span class="text-primary" style="font-weight: bolder; font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px; color: rgb(0, 83, 139) !important;">Cabal Ltda.</span><span style="color: rgb(33, 37, 41); font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px;">&nbsp;El Sistema CABAL de Tarjetas de Crédito fue fundado el 20 de Noviembre del año 1.980.</span><br style="color: rgb(33, 37, 41); font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px;"><div><span style="color: rgb(33, 37, 41); font-family: Calibri, -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 18.4px;">A partir del año 1.989 se encuentra operando en el país, posicionándose como la principal del sector Cooperativo Nacional.</span></div>	\N	\N	\N	\N
\.


--
-- Data for Name: custom_file; Type: TABLE DATA; Schema: public; Owner: cblwebadm
--

COPY public.custom_file (id, filename, format, url) FROM stdin;
\.


--
-- Data for Name: customfile; Type: TABLE DATA; Schema: public; Owner: cblwebadm
--

COPY public.customfile (id, filename, format, url) FROM stdin;
\.


--
-- Data for Name: faq; Type: TABLE DATA; Schema: public; Owner: cblwebadm
--

COPY public.faq (id, question, answer, "order", category, created_at, modified_at, created_by, modified_by) FROM stdin;
\.


--
-- Data for Name: news; Type: TABLE DATA; Schema: public; Owner: cblwebadm
--

COPY public.news (id, content, header_image, title, created_at, modified_at, created_by, modified_by) FROM stdin;
\.


--
-- Data for Name: partner; Type: TABLE DATA; Schema: public; Owner: cblwebadm
--

COPY public.partner (id, name, url, logo, "position", created_at, modified_at, created_by, modified_by) FROM stdin;
3	Coop. De Profesionales de la Salud	https://www.coomecipar.coop.py	coomecipar.png	\N	\N	\N	\N	\N
5	Coop. Medalla Milagrosa	https://www.medalla.coop.py/	medalla.png	\N	\N	\N	\N	\N
6	Coop. San Lorenzo	http://www.sanlorenzo.coop.py	sanlorenzo.jpg	\N	\N	\N	\N	\N
7	Coop. Mercado 4	http://coopmer4.coop.py	mercado4.png	\N	\N	\N	\N	\N
8	Caja Mutual de Coop. Del Paraguay	http://www.cmcp.org.py	cajamutual.png	\N	\N	\N	\N	\N
9	Coop. Sagrados Corazones	http://sagrados.coop.py/	sagradoscorazones.png	\N	\N	\N	\N	\N
10	Coop. San Juan Bautista	http://www.coopersanjuba.com.py	coopesanjuba.png	\N	\N	\N	\N	\N
11	Coop. Del Ñeembucu	http://www.coodene.coop.py	codene.png	\N	\N	\N	\N	\N
12	Coop. Luque	http://www.coopluque.com.py	luque.jpg	\N	\N	\N	\N	\N
13	Coop. Yoayu - Fipsa	http://www.yoayu.coop.py/yoayu/	yoayu.png	\N	\N	\N	\N	\N
14	Coop. Mburicao	http://mburicao.coop.py/es/	mburicao.png	\N	\N	\N	\N	\N
16	Coop. Nazareth	http://www.nazareth.com.py	nazareth.png	\N	\N	\N	\N	\N
17	Coop. Serrana	http://www.serrana.com.py	serrana.png	\N	\N	\N	\N	\N
18	Coop. Don Bosco	http://www.donbosco.coop.py/	donbosco.png	\N	\N	\N	\N	\N
19	Coop. Capiata	http://www.capiata.coop.py	capiata.png	\N	\N	\N	\N	\N
20	Banco Continental	http://www.bancontinental.com.py	continental.png	\N	\N	\N	\N	\N
21	Coop. De vendedores de repuestos y Afines	http://www.coopavra.coop.py	coopavra.png	\N	\N	\N	\N	\N
22	Coop. De Ex. Alumnos del Colegio San José	http://www.coopexsanjo.org.py/	exsanjo.jpg	\N	\N	\N	\N	\N
23	Coop. Nuestra Señora del Carmen	http://www.coopnsc.coop.py	carmen.png	\N	\N	\N	\N	\N
24	Banco Amambay S.A.	https://www.bancobasa.com.py	basa.png	\N	\N	\N	\N	\N
25	Coop. Ayacape	http://www.ayacape.coop.py	ayacape.png	\N	\N	\N	\N	\N
27	Coop. De Sanidad Militar	http://www.coopersam.coop.py	coopersam.jpg	\N	\N	\N	\N	\N
28	Coop. Ñemby	http://www.coopnemby.coop.py/	coopnemby.png	\N	\N	\N	\N	\N
30	Coop. De Villeta	http://www.credivill.coop.py	credivill.png	\N	\N	\N	\N	\N
32	Coop. De Funcionarios de la Entidad Binacional Yacyreta	http://www.coofy.coop.py	coofy.png	\N	\N	\N	\N	\N
33	Coop. De la Aviación	http://copavic.blogspot.com	copavic.jpg	\N	\N	\N	\N	\N
34	Coop. Del Barrio Jara	http://barriojarense.com.py	barriojarense.jpg	\N	\N	\N	\N	\N
35	Coop. Santisima Trinidad	http://www.coosatri.coop.py	coosatri.png	\N	\N	\N	\N	\N
36	Coop. De Funcionarios de la Ande	https://www.cooperande.coop.py	cooperande.jpg	\N	\N	\N	\N	\N
38	Caja de Jubilaciones y Pensiones del personal de la Ande	http://www.cajaande.gov.py	cajaande.png	\N	\N	\N	\N	\N
40	Cooperativa de Fernando de la Mora	http://www.coofedelmo.org.py	coofedelmo.jpg	\N	\N	\N	\N	\N
4	Coop. de Graduados en Ciencias Económicas	http://www.coopec.coop.py	\N	\N	\N	\N	\N	\N
15	Coop. Primer Presidente		\N	\N	\N	\N	\N	\N
26	Coop. Del Personal Retirado de la Policía Nacional		\N	\N	\N	\N	\N	\N
29	Coop. De Funcionarios	http://www.kaarupora.coop.py	\N	\N	\N	\N	\N	\N
31	Coop. Julián Augusto Saldivar		\N	\N	\N	\N	\N	\N
37	Coop. De Oficiales	https://www.8demarzo.coop.py	\N	\N	\N	\N	\N	\N
39	Financiera Bella Vista S.A.		\N	\N	\N	\N	\N	\N
41	Coop. Laguna Sati	http://www.lagunasati.coop.py		\N	\N	\N	\N	\N
1	Coop. De Egresados Universitarios	http://www.cu.coop.py	1571405189408Screen Shot 2019-10-17 at 12.30.09 PM.png\n	0	\N	\N	\N	\N
2	Coop. Paraguaya de la Construcción	http://www.copacons.coop.py	1571677044597Screen Shot 2019-10-14 at 11.24.35 PM.png\n	\N	\N	\N	\N	\N
\.


--
-- Data for Name: slide; Type: TABLE DATA; Schema: public; Owner: cblwebadm
--

COPY public.slide (title, image, link, subtitle, id, "order", created_at, modified_at, created_by, modified_by) FROM stdin;
Cuando volvés a Cabal	15729245279002-2.jpg		Volvés con todo	10	\N	\N	\N	\N	\N
Cuando comprás con cabal 2	15729245380453.jpg		Pasan cosas buenas 2	8	\N	\N	\N	\N	\N
Cuando comprás con Cabal	1572976267487ScreenShot2019-11-04at9.57.24AM.png	http://cabal.coop.py	Pasan cosas nuevas	9	\N	\N	\N	\N	\N
\.


--
-- Data for Name: staff; Type: TABLE DATA; Schema: public; Owner: cblwebadm
--

COPY public.staff (id, division, name, picture, "position", created_at, modified_at, created_by, modified_by) FROM stdin;
12	directors	Ramiro	\N	Vocal Primero	\N	\N	\N	\N
13	directors	Pedro	\N	Vocal 	\N	\N	\N	\N
14	directors	Raul	\N	Segundo	\N	\N	\N	\N
15	directors	ultimo caso	\N	\N	\N	\N	\N	\N
16	directors	Juan Carrillo	\N	Gerente de TI	\N	\N	\N	\N
17	directors	dsasdasd	\N	\N	\N	\N	\N	\N
11	directors	Juan	1571676960387image008.jpg\n	Director General	\N	\N	\N	\N
18	\N		\N	\N	\N	\N	\N	\N
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: cblwebadm
--

COPY public."user" (username, password, name, email, created_at, modified_at, created_by, modified_by) FROM stdin;
admin	admin	Admin	rodrigo.cantero@webparaguay.com	\N	\N	\N	\N
\.


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: cblwebadm
--

SELECT pg_catalog.setval('public.hibernate_sequence', 18, true);


--
-- Name: content content_pk; Type: CONSTRAINT; Schema: public; Owner: cblwebadm
--

ALTER TABLE ONLY public.content
    ADD CONSTRAINT content_pk PRIMARY KEY (identifier);


--
-- Name: custom_file custom_file_pkey; Type: CONSTRAINT; Schema: public; Owner: cblwebadm
--

ALTER TABLE ONLY public.custom_file
    ADD CONSTRAINT custom_file_pkey PRIMARY KEY (id);


--
-- Name: news news_pkey; Type: CONSTRAINT; Schema: public; Owner: cblwebadm
--

ALTER TABLE ONLY public.news
    ADD CONSTRAINT news_pkey PRIMARY KEY (id);


--
-- Name: partner partner_pk; Type: CONSTRAINT; Schema: public; Owner: cblwebadm
--

ALTER TABLE ONLY public.partner
    ADD CONSTRAINT partner_pk PRIMARY KEY (id);


--
-- Name: slide slide_pk; Type: CONSTRAINT; Schema: public; Owner: cblwebadm
--

ALTER TABLE ONLY public.slide
    ADD CONSTRAINT slide_pk PRIMARY KEY (id);


--
-- Name: staff staff_pkey; Type: CONSTRAINT; Schema: public; Owner: cblwebadm
--

ALTER TABLE ONLY public.staff
    ADD CONSTRAINT staff_pkey PRIMARY KEY (id);


--
-- Name: user user_pk; Type: CONSTRAINT; Schema: public; Owner: cblwebadm
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pk PRIMARY KEY (username);


--
-- PostgreSQL database dump complete
--

